import imghdr
import json
from os import listdir
from os.path import isfile
from argparse import ArgumentParser
#Adding command line argument
parser = ArgumentParser()
parser.add_argument("-f", "--folder", dest="pathToFile", help="Input folder path to test!")
args = parser.parse_args()
pathToFile = args.pathToFile
#Creating lists for both pictures and ".json" files
picturesList = [file.split('.')[0] for file in listdir(pathToFile) if isfile(file) and imghdr.what(file) != None]
def is_json(myjson):
	try:
		json_object = json.loads(myjson)
	except ValueError, e:
		return False
	return True
jsonList = []
for fileJson in listdir(pathToFile):
	if isfile(fileJson):
		readJsonFile = open(fileJson, 'r')
		jsonFileValue = readJsonFile.read()
		if is_json(jsonFileValue) == True:
			fileJson = fileJson.split('.')[0]
			jsonList.insert(0, fileJson)
#Comparing two lists to define differences
def Diff(li1, li2): 
    return (list(set(li1) - set(li2))) 
testJpgForJson = Diff(picturesList, jsonList) 
#Retrieving a result
if len(testJpgForJson) == 0:
	print("Congratulations, test is successful")
elif len(testJpgForJson) != 0:
	print("Check the following items:")
	print(testJpgForJson)
